# Google Suite

Support tools related to Google Suite (gmail, Google Docs, etc.)

## Tools

### Agenda Maker

#### Purpose

When using a Google Doc for meeting agendas and notes, we commonly create in
the doc an agenda template that we then copy and paste prior to each meeting
to create the agenda. The purpose of these Google Apps Scripts is to make that
process easier by providing a menu in the document's menu bar for creating
the new agenda with a single click.

#### Usage in a Google Doc that has an Agenda Maker

1. Find the menu bar item "Create new agenda"
1. Select the appropriate action from the menu
   1. (wait a bit, it may take several seconds)
   1. If you're using it for the first time,
      you'll get a permission prompt. After granting permission, you will need
      to repeat the action that prompted the permission dialog.


#### Adding an Agenda Maker to a Google Doc

1. Open the doc to which you wish to add the agenda maker
1. Go to "Tools -> Script editor"
1. Remove the initial contents of the script
1. Copy the entire contents of one of the Agenda Maker files in this repository
   into the script editor
1. Modify the following to meet your needs:
   1. Change the regional constants at the top of the script according to
      how your meetings are structured. Remove them if regions are not in use.
   1. Change the getNextFriday() function to find the appropriate next meeting
      day as per your meeting schedule. *Note that the calculation in this
      function ONLY works for Fridays.*
   1. If you wish to use different start and end markers for your agenda
      template, change the search() calls to look for desired markers.
   1. Change the replaceText() calls according to your meeting's needs.
   1. Change the onOpen() function at the bottom of the script to create
      the document menu according to your needs, and change the functions that
      it calls.
1. Create the agenda template in your document:
   1. The script expects the template to be separated from the insertion point
      of the real agenda by a horizontal rule (line). If you want the template
      to be somewhere else, or not to have the horizontal rule after it,
      you'll need to modify the code to position the cursor appropriately
      before beginning to insert the agenda into the doc.
   1. Enter the start and end markers before and after the agenda template,
      respectively.
1. Save the script
1. Reload the document so that it will run the onOpen() function
1. Test your menu items, and expect that when you first try one of them, Google
   will ask you for permission. After granting permission, you will need to repeat
   the action that prompted the permission dialog.
